# @patchman/bottom_bar

Custom bottom bar for Hammerfest.

Requires a BottomBar.json configuration file.

The structure is as follow:

```javascript
{
    "mode":{
        "option": [
            {"name":"module name", arguments}
        ]
    },
    "default":{
        "default": [
        ]
    }
}
```

Here is an example using custom modules. In this example, multi is an option rather than a mode.
```javascript
{
    "solo":{
        "multi": [
            {"name":"PartyBackground"},
            {"name":"PartyScore"},
            {"name":"CustomLives", "x":[47, 313, 96, 266], "reversed":[false, false, false, false]}
        ]
    },
    "default":{
        "default": [
            {"name":"SoloBackground"},
            {"name":"SoloScore"},
            {"name":"SoloLevel"},
            {"name":"CustomLives", "x":[85], "reversed":[false]}
        ]
    }
}
```
'default' is used where the mode or option is not specified.
List of provided modules:
- SoloScore : the official module to display the score in solo
- SoloBackground : the official module to display the bottom bar background in solo (must be first in the list)
- SoloLevel : the official module to display the level in solo
- SoloLives : the official module to display the lives in solo
- MultiScore : the official module to display the score in multi
- MultiBackground : the official module to display the bottom bar background (must be first in the list)
- MultiLives : the official module to display the lives in multi
- CustomLives : The module that improves the lives module. Arguments are x (array of int, for the pictures) and reversed (array of bool)
- LevelTimer : The module to display the time spent in the level so far. Argument is x (int, x value on the axis)
- GlobalTimer : The module to display the time spent in the game so far. Argument is x (int, x value on the axis)
- QuickTimer : The module to display the remaining time before Quick. Argument is x (int, x value on the axis)
- FireballTimer : The module to display the remaining time before the fireball. Argument is x (int, x value on the axis)
