# 0.2.2 (2022-05-11)

- **[Fix]** Improve performances of `FireballTimer` and `QuickTimer`

# 0.2.1 (2021-05-13)

- **[Fix]** Update required patchman version to `0.10.6`.
- **[Fix]** Fix `FireballTimer`, `QuickTimer`, `GlobalTimer`, `LevelTimer`

# 0.2.0 (2021-04-22)

- **[Breaking change]** Update to `patchman@0.10.4`.
- **[Internal]** Update to Yarn 2.

# 0.1.0

- **[Feature]** First release.
