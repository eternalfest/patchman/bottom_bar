package bottom_bar;

import bottom_bar.modules.BottomBarModule;
import hf.mode.GameMode;
import hf.Hf;
import hf.gui.GameInterface;

@:hfTemplate
class BottomBarInterface extends GameInterface {

    private var single_modules: Array<BottomBarModule>;
    private var multi_modules: Array<BottomBarModule>;
    private var modules: Array<BottomBarModule>;

    public function new(hf: Hf, game: GameMode) {
        this.modules = new Array<BottomBarModule>();
        super(hf, game);
    }

    public function init(): Void {
        for(i in 0...this.modules.length) {
            this.modules[i].init(this);
        }
        var players = this.game.getPlayerList();
        var cpt: Int = cast Math.max(4, players.length);
        for(i in 0...cpt)
            this.clearExtends(i);
    }
    public override function initSingle(): Void {
    }

    public override function initMulti(): Void {
    }

    public override function initTime(): Void {
    }

    public override function setLives(pId: Int, lives: Int): Void {
        var call_super: Bool;
        call_super = true;
        for(i in 0...this.modules.length) {
            call_super = this.modules[i].setLives(pId, lives, this) && call_super;
        }
        if(call_super)
            super.setLives(pId, lives);
    }

    public override function update(): Void {
        var call_super: Bool;
        call_super = true;
        for(i in 0...this.modules.length) {
            call_super = this.modules[i].update(this) && call_super;
        }
        if(call_super)
            super.update();
    }

    public override function setLevel(id: Int): Void {
        var call_super: Bool;
        call_super = true;
        for(i in 0...this.modules.length) {
            call_super = this.modules[i].setLevel(id, this) && call_super;
        }
        if(call_super)
            super.setLevel(id);
    }

    public override function setScore(pid: Int, value: Int): Void {
        var call_super: Bool;
        call_super = true;
        for(i in 0...this.modules.length) {
            call_super = this.modules[i].setScore(pid, value, this) && call_super;
        }
        if(call_super)
            super.setScore(pid, value);
    }

    public override function getExtend(pid: Int, id: Int): Void {
        var call_super: Bool;
        call_super = true;
        for(i in 0...this.modules.length) {
            call_super = this.modules[i].getExtend(pid, id, this) && call_super;
        }
        if(call_super)
            super.getExtend(pid, id);
    }


    public override function hideLevel(): Void {
        var call_super: Bool;
        call_super = true;
        for(i in 0...this.modules.length) {
            call_super = this.modules[i].hideLevel(this) && call_super;
        }
        if(call_super)
            super.hideLevel();
    }


    public function addModule(module: BottomBarModule): Void {
        this.modules.push(module);
    }


    public function updateModules(action: String, obj: Dynamic) {
        for(i in 0...this.modules.length) {
            this.modules[i].updateModule(action, obj, this);
        }
    }
}
