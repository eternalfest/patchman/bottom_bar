package bottom_bar;

import etwin.Obfu;
import hf.mode.GameMode;
import bottom_bar.modules.BottomBarModuleFactory;
import bottom_bar.modules.BottomBarModule;
import bottom_bar.json_reader.JSONReader;

@:build(patchman.Build.di())
class BottomBarConfig {
    private static var DEFAULT: String = Obfu.raw("default");
    public var modules: Map<String, Map<String, Array<BottomBarModule>>>;
    public var json_object: Map<String, Dynamic>;
    public function new(data: Dynamic) {
        modules = new Map<String, Map<String, Array<BottomBarModule>>>();
        json_object = new JSONReader().build(data);
    }

    public function load() {
        for(key in json_object.keys()) {
            modules[key] = loadOptions(json_object[key]);
        }
    }

    private function loadOptions(map: Map<String, Array<Dynamic>>): Map<String, Array<BottomBarModule>> {
        var options = new Map<String, Array<BottomBarModule>>();
        for(key in map.keys()) {
            options[key] = loadModules(map[key]);
        }
        return options;
    }

    private function loadModules(map: Array<Dynamic>): Array<BottomBarModule> {
        var modules = new Array<BottomBarModule>();
        for(module in map) {
            modules.push(BottomBarModuleFactory.get().create(module));
        }
        return modules;
    }

    public function getModules(game: GameMode): Array<BottomBarModule> {
        for(i in modules.keys()) {
            if(game.manager.isMode(i) ||game.manager.isMode("$" + i) ) {
                for(j in modules[i].keys()) {
                    if(game.root.GameManager.CONFIG.hasOption(j) || game.root.GameManager.CONFIG.hasOption("$" + j))
                        return modules[i][j];
                }
                return modules[i][DEFAULT] != null ? modules[i][DEFAULT] : modules[DEFAULT][DEFAULT];
            }
        }
        return modules[DEFAULT][DEFAULT];
    }

    @:diFactory
    public static function fromHml(data: patchman.module.Data): BottomBarConfig {
        return new BottomBarConfig(data.get(Obfu.raw("BottomBar")));
    }
}
