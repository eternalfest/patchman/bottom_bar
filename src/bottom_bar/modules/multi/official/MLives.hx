package bottom_bar.modules.multi.official;

class MLives extends BottomBarModule {
    public function new(data: Dynamic) {
    }

    public override function init(bottomBar:BottomBarInterface): Void {
        bottomBar.currentLives = new Array();
        var players = (bottomBar.game.getPlayerList());
        bottomBar.lives = new Array();
        for(i in 0...players.length) {
            var player = players[i];
            bottomBar.lives[i] = new Array();
            bottomBar.currentLives[i] = 0;
            bottomBar.setLives(i, player.lives);
        }
    }
}
