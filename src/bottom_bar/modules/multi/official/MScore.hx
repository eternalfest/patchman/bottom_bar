package bottom_bar.modules.multi.official;

class MScore extends BottomBarModule {
    public function new(data: Dynamic) {
    }

    public override function init(bottomBar:BottomBarInterface): Void {
        bottomBar.scores = [untyped bottomBar.mc.score0, untyped bottomBar.mc.score1];
        bottomBar.fakeScores = new Array();
        bottomBar.realScores = new Array();
        var players = (bottomBar.game.getPlayerList());
        for(i in 0...players.length) {
            bottomBar.fakeScores[i] = 0;
            bottomBar.realScores[i] = 0;
            bottomBar.setScore(i, players[i].score);
            untyped bottomBar.scores[i].textColor = bottomBar.game.root.Data.BASE_COLORS[0];
            bottomBar.game.root.FxManager.addGlow(bottomBar.scores[i], bottomBar.game.root.gui.GameInterface.GLOW_COLOR, 2);
        }
    }
}
