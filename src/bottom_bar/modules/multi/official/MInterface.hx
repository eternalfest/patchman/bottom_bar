package bottom_bar.modules.multi.official;

//import etwin.flash.DynamicMovieClip;

class MInterface extends BottomBarModule {
    public function new(data: Dynamic) {
    }

    public override function init(bottomBar: BottomBarInterface): Void {
        bottomBar.fl_multi = cast true;
        bottomBar.mc.removeMovieClip();
        bottomBar.mc = ((cast bottomBar.game.depthMan.attach("hammer_interf_game", bottomBar.game.root.Data.DP_TOP)): Dynamic);
        bottomBar.mc._x = -bottomBar.game.xOffset;
        bottomBar.mc._y = cast bottomBar.game.root.Data.DOC_HEIGHT;
        bottomBar.mc.gotoAndStop("2");
        bottomBar.mc.cacheAsBitmap = true;
        bottomBar.letters = new Array();
        bottomBar.letters[0] = new Array();
        var mc: Dynamic;
        mc = cast bottomBar.mc;
        bottomBar.letters[0].push(mc.letter0_0);
        bottomBar.letters[0].push(mc.letter0_1);
        bottomBar.letters[0].push(mc.letter0_2);
        bottomBar.letters[0].push(mc.letter0_3);
        bottomBar.letters[0].push(mc.letter0_4);
        bottomBar.letters[0].push(mc.letter0_5);
        bottomBar.letters[0].push(mc.letter0_6);
        bottomBar.letters[1] = new Array();
        bottomBar.letters[1].push(mc.letter1_0);
        bottomBar.letters[1].push(mc.letter1_1);
        bottomBar.letters[1].push(mc.letter1_2);
        bottomBar.letters[1].push(mc.letter1_3);
        bottomBar.letters[1].push(mc.letter1_4);
        bottomBar.letters[1].push(mc.letter1_5);
        bottomBar.letters[1].push(mc.letter1_6);
    }
}
