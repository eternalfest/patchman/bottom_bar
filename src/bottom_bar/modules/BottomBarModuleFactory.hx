package bottom_bar.modules;

import etwin.Obfu;

import bottom_bar.modules.custom.FireballTimer;
import bottom_bar.modules.custom.QuickTimer;
import bottom_bar.modules.custom.GlobalTimer;
import bottom_bar.modules.custom.LevelTimer;
import bottom_bar.modules.custom.ReducedLives;
import bottom_bar.modules.multi.official.MLives;
import bottom_bar.modules.single.official.SLives;
import bottom_bar.modules.multi.official.MInterface;
import bottom_bar.modules.multi.official.MScore;
import bottom_bar.modules.single.official.SLevel;
import bottom_bar.modules.single.official.SInterface;
import bottom_bar.modules.single.official.SScore;

class BottomBarModuleFactory {
    private static var instance: BottomBarModuleFactory;

    private var modules: Map<String, Dynamic -> BottomBarModule>;
    public function new() {
        modules = new Map<String, Dynamic -> BottomBarModule>();
        addModule(Obfu.raw("SoloScore"), function(data: Dynamic) return new SScore(data));
        addModule(Obfu.raw("SoloBackground"), function(data: Dynamic) return new SInterface(data));
        addModule(Obfu.raw("SoloLevel"), function(data: Dynamic) return new SLevel(data));
        addModule(Obfu.raw("MultiScore"), function(data: Dynamic) return new MScore(data));
        addModule(Obfu.raw("MultiBackground"), function(data: Dynamic) return new MInterface(data));
        addModule(Obfu.raw("SoloLives"), function(data: Dynamic) return new SLives(data));
        addModule(Obfu.raw("MultiLives"), function(data: Dynamic) return new MLives(data));
        addModule(Obfu.raw("CustomLives"), function(data: Dynamic) return new ReducedLives(data));
        addModule(Obfu.raw("LevelTimer"), function(data: Dynamic) return new LevelTimer(data));
        addModule(Obfu.raw("GlobalTimer"), function(data: Dynamic) return new GlobalTimer(data));
        addModule(Obfu.raw("QuickTimer"), function(data: Dynamic) return new QuickTimer(data));
        addModule(Obfu.raw("FireballTimer"), function(data: Dynamic) return new FireballTimer(data));
    }

    public function create(module: Map<String, Dynamic>): BottomBarModule {
        return modules[module[Obfu.raw("name")]](module);
    }

    public static function get(): BottomBarModuleFactory {
        if(instance == null)
            instance = new BottomBarModuleFactory();
        return instance;
    }

    public function addModule(key: String, lambda: Dynamic -> BottomBarModule) {
        modules[key] = lambda;
    }

    public function get_list(): Iterator<String> {
        return modules.keys();
    }
}
