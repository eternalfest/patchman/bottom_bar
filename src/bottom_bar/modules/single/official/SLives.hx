package bottom_bar.modules.single.official;

class SLives extends BottomBarModule {
    public function new(data: Dynamic) {
    }

    public override function init(bottomBar:BottomBarInterface): Void {
        var player = (bottomBar.game.getPlayerList())[0];
        bottomBar.currentLives = [0];
        bottomBar.lives = [[]];
        bottomBar.setLives(0, player.lives);
    }
}
