package bottom_bar.modules.single.official;

import hf.FxManager;
class SScore extends BottomBarModule {
    public function new(data: Dynamic) {
    }

    public override function init(bottomBar:BottomBarInterface): Void {
        bottomBar.scores = [untyped bottomBar.mc.score0];
        bottomBar.fakeScores = [0];
        bottomBar.realScores = [0];
        var player = (bottomBar.game.getPlayerList())[0];
        bottomBar.setScore(0, player.score);
        bottomBar.setLives(0, player.lives);
        bottomBar.clearExtends(0);
        untyped bottomBar.scores[0].textColor = bottomBar.game.root.Data.BASE_COLORS[0];
        bottomBar.game.root.FxManager.addGlow(bottomBar.scores[0], bottomBar.game.root.gui.GameInterface.GLOW_COLOR, 2);
    }
}
