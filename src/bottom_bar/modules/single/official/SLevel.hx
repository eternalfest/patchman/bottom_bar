package bottom_bar.modules.single.official;

class SLevel extends BottomBarModule {
    public function new(data: Dynamic) {
    }

    public override function init(bottomBar:BottomBarInterface): Void {
        bottomBar.mc._visible = true;
        bottomBar.level = untyped bottomBar.mc.level;
    }
}
