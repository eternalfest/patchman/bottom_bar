package bottom_bar.modules.custom;

import etwin.Obfu;

class QuickTimer extends BottomBarModule {
    public var timerSprite: Dynamic;
    public var x: Int;

    public function new(module: Map<String, Dynamic>) {
        x = module[Obfu.raw("x")];
    }

    public override function init(bottomBar:BottomBarInterface): Void {
        var quickSprite: Dynamic = bottomBar.game.depthMan.attach('hammer_interf_inGameMsg', bottomBar.game.root.Data.DP_TOP + 1);
        quickSprite.field._visible = true;
        quickSprite.field._width = 400;
        quickSprite.field._xscale = 115;
        quickSprite.field._yscale = 115;
        quickSprite.field._x = x;
        quickSprite.field._y = 499;
        quickSprite.field.text = "!";
        quickSprite.field.textColor = 0xEE5900;
        quickSprite.label._visible = false;
        bottomBar.game.root.FxManager.addGlow(quickSprite.field, 7366029, 2);

        timerSprite = bottomBar.game.depthMan.attach('hammer_interf_inGameMsg', bottomBar.game.root.Data.DP_TOP + 1);
        timerSprite.field._visible = true;
        timerSprite.field._width = 400;
        timerSprite.field._xscale = 115;
        timerSprite.field._yscale = 115;
        timerSprite.field._x = x + 10;
        timerSprite.field._y = 499;
        timerSprite.label._visible = false;
        bottomBar.game.root.FxManager.addGlow(timerSprite.field, 7366029, 2);
    }

    public override function update(bottomBar:BottomBarInterface): Bool {
        var text: String;
        if (bottomBar.game.huState >= 1) {
            text = "0.0";
        }
        else {
            var elapsedTime: Float = Math.max(((bottomBar.game.root.Data.HU_STEPS[0] / bottomBar.game.diffFactor - bottomBar.game.huTimer) / bottomBar.game.root.Data.SECOND), 0);
            text = Std.string(Math.round(elapsedTime * 10) / 10);
            if (Math.round(elapsedTime * 10) % 10 == 0)
                text += ".0";
        }
        if (text != timerSprite.field.text) {
            timerSprite.field.text = text;
        }
        return true;
    }
}
