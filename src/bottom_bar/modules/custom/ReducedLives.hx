package bottom_bar.modules.custom;

import etwin.Obfu;

class ReducedLives extends BottomBarModule {
    public var posLives: Array<Int>;
    public var posGraphics: Array<Int>;
    public var position: Int;
    public var positions: Array<Dynamic>;
    public var reversed: Array<Bool>;
    public var lives: Array<Dynamic>;
    public var sprites: Array<Dynamic>;
    public var max_lives:Int;
    public var multi_x: Array<Int>;

    public function new(module: Map<String, Dynamic>) {
        lives = new Array<Dynamic>();
        reversed = new Array<Bool>();
        posLives = new Array<Int>();
        posGraphics = new Array<Int>();
        sprites = new Array<Dynamic>();
        max_lives = 4;
        multi_x = module[Obfu.raw("x")];
        reversed = module[Obfu.raw("reversed")];
    }

    public override function init(bottomBar:BottomBarInterface): Void {
        var players = (bottomBar.game.getPlayerList());
        for(i in 0...lives.length) {
            lives[i].removeMovieClip();
            sprites[i].removeMovieClip();
        }
        positions = [];
        posLives = [];
        posGraphics = [];
        lives = [];
        sprites = [];
        var size = players.length;
        if(size > max_lives)
            size = max_lives;
        for (i in 0...size)
        {
            var posX = multi_x[i];
            this.posLives.push((!reversed[i] ? posX : posX + 10));
            this.posGraphics.push((!reversed[i] ? posX + 28 : posX));
            this.lives.push(bottomBar.game.depthMan.attach('hammer_interf_inGameMsg', bottomBar.game.root.Data.DP_TOP + 1));
            this.lives[i].field._visible = false;
            this.lives[i].label._visible = false;
            this.lives[i].field._width = 400;
            this.lives[i].field._xscale = 115;
            this.lives[i].field._yscale = 115;
            this.lives[i].field._y = 499;
            bottomBar.game.root.FxManager.addGlow(this.lives[i], 7366029, 2);

/*
            this.sprites.push(bottomBar.game.root.Std.attachMC(bottomBar.mc, 'hammer_interf_life', bottomBar.game.manager.uniq++));
            this.sprites[i]._x = this.posGraphics[i];
            this.sprites[i]._y = -15;
            this.sprites[i]._xscale = 65;
            this.sprites[i]._yscale = 65;*/
            this.lives[i].field._visible = true;
            this.setLives(i, players[i].lives, bottomBar);
        }
    }

    public override function setLives(pid: Int, lives: Int, bottomBar: BottomBarInterface): Bool {
        if(lives >= 0 && posLives.length > 0) {
            this.lives[pid].field.text = lives;
            this.lives[pid].field._x = this.posLives[pid] - 5 * (lives > 9 ? 1 : 0);
            this.sprites[pid].removeMovieClip();
            this.sprites[pid] = bottomBar.game.root.Std.attachMC(bottomBar.mc, 'hammer_interf_life', bottomBar.game.manager.uniq++);
            this.sprites[pid]._x = this.posGraphics[pid];
            this.sprites[pid]._y = -15;
            this.sprites[pid]._xscale = 65;
            this.sprites[pid]._yscale = 65;
        }
        return false;
    }
}
