package bottom_bar.modules.custom;

import etwin.Obfu;

class Timer extends BottomBarModule {
    public var timerSprite: Dynamic;
    public var x: Int;

    public function new(module: Map<String, Dynamic>) {
        x = module[Obfu.raw("x")];
    }

    public function updateTimer(hours: Int, minutes: Int, seconds: Int, thousandths: Int) {
        var text: String = "";
        if (hours < 10)
            text += "0";
        text += hours + ":";
        if (minutes < 10)
            text += "0";
        text += minutes + ":";
        if (seconds < 10)
            text += "0";
        text += seconds + ".";

        if (thousandths < 100)
            text += "0";
        if (thousandths < 10)
            text += "0";
        text += "" + thousandths;
        if (timerSprite.field.text != text) {
            timerSprite.field.text = text;
        }
        timerSprite.field._visible = true;
    }
}
