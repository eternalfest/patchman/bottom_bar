package bottom_bar.modules.custom;
class GlobalTimer extends Timer {

    public var elapsedTime: Int;
    public function new(module: Dynamic) {
        super(module);
    }

    public override function init(bottomBar:BottomBarInterface): Void {
        elapsedTime = 0;
        timerSprite = bottomBar.game.depthMan.attach('hammer_interf_inGameMsg', bottomBar.game.root.Data.DP_TOP + 1);
        timerSprite.field._visible = false;
        timerSprite.label._visible = false;
        timerSprite.field._width = 400;
        timerSprite.field._xscale = 115;
        timerSprite.field._yscale = 115;
        timerSprite.field._x = x;
        timerSprite.field._y = 499;
        bottomBar.game.root.FxManager.addGlow(timerSprite, 7366029, 2);
        updateTimer(0, 0, 0, 0);
    }

    public override function update(bottomBar: BottomBarInterface): Bool {
        if (!bottomBar.game.fl_pause && !bottomBar.game.fl_lock) {
            elapsedTime++;
            var timeElapsedInSecondsInLevel: Int = Std.int(elapsedTime / 40);
            var levelTimerSeconds: Int = timeElapsedInSecondsInLevel % 60;
            var levelTimerMinutes: Int = cast (((timeElapsedInSecondsInLevel - levelTimerSeconds) % 3600) / 60);
            var levelTimerHours: Int = cast ((timeElapsedInSecondsInLevel - levelTimerMinutes * 60 - levelTimerSeconds) / 3600);
            var levelTimerThousandths: Int = 25 * (elapsedTime % 40);
            updateTimer(levelTimerHours, levelTimerMinutes, levelTimerSeconds, levelTimerThousandths);
        }
        return true;
    }
}
