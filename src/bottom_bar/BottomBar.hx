package bottom_bar;


import bottom_bar.modules.BottomBarModule;
import bottom_bar.BottomBarConfig;

import patchman.PatchList;
import patchman.IPatch;
import patchman.Ref;
import hf.Hf;
import hf.mode.GameMode;

@:build(patchman.Build.di())
class BottomBar {

    @:diExport
    public var patch(default, null): IPatch;


    private var single_modules: Array<BottomBarModule>;
    private var multi_modules: Array<BottomBarModule>;
    private var bar: BottomBarInterface;
    private var config: BottomBarConfig;

    private static var instance: BottomBar;

    public function new(patches: Array<IPatch>, config: BottomBarConfig) {
        single_modules = [];
        bar = null;
        multi_modules = [];
        this.config = config;
        instance = this;
        patches.push(Ref.auto(GameMode.initInterface).wrap(function(h: Hf, self: GameMode, old) {
            (self.gi: hf.gui.GameInterface).destroy();
            self.gi = new BottomBarInterface(self.root, self);
            bar = self.gi;
            initialize();
        }));
        this.patch = new PatchList(patches);
    }

    public static function get(): BottomBar {
        return instance;
    }

    public function updateModules(action: String, obj: Dynamic) {
        bar.updateModules(action, obj);
    }

    private function initialize() {
        config.load();
        var modules: Array<BottomBarModule> = config.getModules(bar.game);
        for(i in modules)
            bar.addModule(i);
        bar.init();
    }

    public function refresh() {
        bar.init();
    }
}
